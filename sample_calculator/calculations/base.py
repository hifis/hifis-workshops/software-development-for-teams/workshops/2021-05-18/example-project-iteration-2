# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
Implements basic framework for calculation implementations.

A calculation is always performed on a sequence of numeric values and
returns a single value. To add a new calculation do the following things:

* Create a new module in sample_calculator.calculations.calculations (e.g., division_calculation).
* Define a string constant which uniquely identifies the calculation (e.g., DIVISION_CALCULATION = "DIVISION")
* Make the name constant available outside the package (see __init__.py for details)
* Subclass :class:`Calculation` (e.g., DivisionCalculation)
* Implement the abstract properties and interface methods:

 * Define the name using the string constant
 * Define the dependent calculations using the string constants (import them from sample_calculator.calculations).
 * Implement the algorithm
 * Indicate errors using :class:`CalculationError`

* Always use the factory function :func:`create_calculation` to obtain a specific instance
* The new calculation is automatically registered in the factory method

"""


import abc
import decimal
import inspect
import logging
import pkgutil

from sample_calculator.calculations import calculations

_LOG = logging.getLogger()
_CALCULATION_CACHE = dict()


def create_calculation(calculation_names):
    """ Returns the required calculation instances.

    It resolves and initializes all dependent calculations
    and provides a common cache for them.

    :param calculation_names: Sequence of the required calculations.

    :returns: Sequence of initialized calculation instances.

    :raises: :class:`CalculationError`
    """

    # Initialize cache
    if not _CALCULATION_CACHE:
        for module_importer, fullname, _ in pkgutil.walk_packages(calculations.__path__, calculations.__name__ + "."):
            module_loader = module_importer.find_module(fullname)
            module = module_loader.load_module(fullname)
            for _, classobj in inspect.getmembers(module):
                try:
                    if classobj != Calculation and Calculation in classobj.mro():
                        _CALCULATION_CACHE[classobj.name] = classobj
                        break
                except AttributeError:
                    pass  # Simply ignoring non-class members

    # Create calculations
    requested_calculations = list()
    cache = dict()
    for calculation_name in calculation_names:
        requested_calculations.append(_create_calculation(calculation_name, list(), cache))
    return requested_calculations


def _create_calculation(calculation_name, visited_calculations, cache):
    """ Initializes a single calculation. """

    if calculation_name in _CALCULATION_CACHE:
        visited_calculations.append(calculation_name)
        calculation_class = _CALCULATION_CACHE[calculation_name]
        dependent_calculations = _create_dependent_calculations(
            calculation_class.dependent_calculations, visited_calculations, cache
        )
        visited_calculations.remove(calculation_name)
        return calculation_class(dependent_calculations, cache)
    raise CalculationError("The calculation '{0}' does not exist.".format(calculation_name))


def _create_dependent_calculations(calculation_names, visited_calculations, cache):
    """ Recursive calls to _create_calculation. Loop detection is performed as well. """

    dependent_calculations = list()
    for calculation_name in calculation_names:
        if calculation_name not in visited_calculations:
            calculation = _create_calculation(calculation_name, visited_calculations, cache)
            dependent_calculations.append(calculation)
        else:
            raise CalculationError("Loop to calculation '{0}' detected.".format(calculation_name))
    return dependent_calculations


class CalculationError(Exception):
    """ Indicates errors during a calculation. """


class Calculation(metaclass=abc.ABCMeta):
    """ Describes the calculation interface. """

    def __init__(self, dependent_calculations, cache):
        """
        The correct instantiation is performed by the factory method.

        :param dependent_calculations: Sequence of the dependent calculation instances.
        :type dependent_calculations: :class:`Calculation`
        :param cache: Common cache which is used for result sharing between
            different top-level calculations.
        :type cache: dict
        """

        self._cache = cache
        self._dependent_calculations = dependent_calculations

    @abc.abstractproperty
    def name(self):
        """ Defines the unique calculation name. """

    @abc.abstractproperty
    def dependent_calculations(self):
        """ Defines the calculations which have to
        be previously performed. Dependent calculations
        are identified by their name. """

    def calculate(self, sample_values):
        """ Performs a calculation. This implementation uses a cache to
        avoid re-calculation of already performed calculations.

        :param sample_values: Sequence of numerical values.
        :type sample_values: list of :class:`decimal.Decimal`

        :returns: Calculated numerical value.
        :rtype: :class:`decimal.Decimal`
        """

        # Handle wrong type
        if sample_values is None:
            raise TypeError("Invalid values.")

        # Handle empty list of sample values
        if not sample_values:
            return decimal.Decimal("NaN")

        # Perform calculation or read it from cache
        if self.name not in self._cache:
            _LOG.debug("Calculating result for calculation %s...", self.name)
            dependent_values = dict()
            for calculation in self._dependent_calculations:
                dependent_values[calculation.name] = calculation.calculate(sample_values)
            self._cache[self.name] = self._calculate(sample_values, dependent_values)
        else:
            _LOG.debug("Reading result for calculation %s from cache...", self.name)
        return self._cache[self.name]

    @abc.abstractmethod
    def _calculate(self, sample_values, dependent_values):
        """ Concrete sub classes must implement the
        calculation algorithm here.

        :param sample_values: Sequence of numerical values.
        :param dependent_values: The dependent are given within a dictionary:
            keys => unique name of the calculation value => result of the calculation.
        """
